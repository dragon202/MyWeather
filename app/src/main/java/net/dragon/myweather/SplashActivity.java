package net.dragon.myweather;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class SplashActivity extends AppCompatActivity {
    View[] view = new View[2];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        view[0] = this.getLayoutInflater().inflate(R.layout.page1,null);
        view[1] = this.getLayoutInflater().inflate(R.layout.page2,null);
        ViewPager viewPager = this.findViewById(R.id.viewpage);
        viewAdapter viewAdapter = new viewAdapter();
        viewPager.setAdapter(viewAdapter);
        Button button = view[1].findViewById(R.id.loginBut);
        button.getBackground().setAlpha(150);
        Button loginBut = view[1].findViewById(R.id.loginBut);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,SelectActivity.class);
                startActivity(intent);
            }
        },2500);
        loginBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SplashActivity.this,SelectActivity.class);
                startActivity(intent);
            }
        });

    }
    public class viewAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return view.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view ==object;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            container.addView(view[position]);
            return view[position];
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView(view[position]);
        }
    }
}