package net.dragon.myweather.entity;

public class LifeResult {
    private String city;
    private Life life;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Life getLife() {
        return life;
    }

    public void setLife(Life life) {
        this.life = life;
    }
}
