package net.dragon.myweather.entity;

public class Life {
    private KongTiao kongtiao;
    private ShuShidu shushidu;
    private ChuangYi chuangyi;
    private GanMao ganmao;
    private YunDong yundong;
    private DaiSan daisan;

    public KongTiao getKongtiao() {
        return kongtiao;
    }

    public void setKongtiao(KongTiao kongtiao) {
        this.kongtiao = kongtiao;
    }

    public ShuShidu getShushidu() {
        return shushidu;
    }

    public void setShushidu(ShuShidu shushidu) {
        this.shushidu = shushidu;
    }

    public ChuangYi getChuangyi() {
        return chuangyi;
    }

    public void setChuangyi(ChuangYi chuangyi) {
        this.chuangyi = chuangyi;
    }

    public GanMao getGanmao() {
        return ganmao;
    }

    public void setGanmao(GanMao ganmao) {
        this.ganmao = ganmao;
    }

    public YunDong getYundong() {
        return yundong;
    }

    public void setYundong(YunDong yundong) {
        this.yundong = yundong;
    }

    public DaiSan getDaisan() {
        return daisan;
    }

    public void setDaisan(DaiSan daisan) {
        this.daisan = daisan;
    }
}
