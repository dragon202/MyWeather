package net.dragon.myweather.entity;

public class WeatherResult {
    private String reason;
    private LifeResult result;
    private int error_code;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public LifeResult getResult() {
        return result;
    }

    public void setResult(LifeResult result) {
        this.result = result;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }
}
