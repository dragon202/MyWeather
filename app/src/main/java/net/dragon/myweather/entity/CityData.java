package net.dragon.myweather.entity;

import java.util.List;

public class CityData {
    private String reason;
    private List<CityResult>result;
    private int error_code;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<CityResult> getResult() {
        return result;
    }

    public void setResult(List<CityResult> result) {
        this.result = result;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }
}
