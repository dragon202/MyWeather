package net.dragon.myweather.entity;

public class ChuangYi {
    private String v;
    private String des;

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
