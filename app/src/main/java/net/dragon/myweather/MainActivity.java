package net.dragon.myweather;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import net.dragon.myweather.Fragment.MeteorologyFragment;
import net.dragon.myweather.Fragment.SetUpFragment;
import net.dragon.myweather.Fragment.TrendsFragment;
import net.dragon.myweather.Fragment.WeatherFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private RadioGroup radioGroup;
    private List<Fragment> fragments = new ArrayList<>();
    MeteorologyFragment mef;
    SetUpFragment suf;
    TrendsFragment trf;
    WeatherFragment wef;
    private  List<View>views;
    RadioButton rb1 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = findViewById(R.id.MainviewPage);
        rb1 = findViewById(R.id.one);
        radioGroup = findViewById(R.id.radiogroup);
        mef = new MeteorologyFragment();
        suf = new SetUpFragment();
        trf = new TrendsFragment();
        wef = new WeatherFragment();



        fragments.add(wef);
        fragments.add(mef);
        fragments.add(trf);
        fragments.add(suf);
        WeatherAdapter weatherAdapter = new WeatherAdapter(getSupportFragmentManager());
        viewPager.setAdapter(weatherAdapter);
        views=radioGroup.getTouchables();
        rb1.setBackgroundResource(R.drawable.shape);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                RadioButton rb2 = findViewById(R.id.two);
                RadioButton rb3 = findViewById(R.id.three);
                RadioButton rb4 = findViewById(R.id.four);
                if (position ==0){
                    rb1.setBackgroundResource(R.drawable.shape);
                    rb2.setBackgroundColor(Color.TRANSPARENT);
                    rb3.setBackgroundColor(Color.TRANSPARENT);
                    rb4.setBackgroundColor(Color.TRANSPARENT);
                    rb1.setTextColor(Color.BLUE);
                    rb2.setTextColor(Color.BLACK);
                    rb3.setTextColor(Color.BLACK);
                    rb4.setTextColor(Color.BLACK);
                }else if (position==1){
                    rb2.setBackgroundResource(R.drawable.shape);
                    rb1.setBackgroundColor(Color.TRANSPARENT);
                    rb3.setBackgroundColor(Color.TRANSPARENT);
                    rb4.setBackgroundColor(Color.TRANSPARENT);
                    rb2.setTextColor(Color.BLUE);
                    rb1.setTextColor(Color.BLACK);
                    rb3.setTextColor(Color.BLACK);
                    rb4.setTextColor(Color.BLACK);
                }else if (position==2){
                    rb3.setBackgroundResource(R.drawable.shape);
                    rb2.setBackgroundColor(Color.TRANSPARENT);
                    rb1.setBackgroundColor(Color.TRANSPARENT);
                    rb4.setBackgroundColor(Color.TRANSPARENT);
                    rb3.setTextColor(Color.BLUE);
                    rb2.setTextColor(Color.BLACK);
                    rb1.setTextColor(Color.BLACK);
                    rb4.setTextColor(Color.BLACK);
                }else if (position==3){
                    rb4.setBackgroundResource(R.drawable.shape);
                    rb2.setBackgroundColor(Color.TRANSPARENT);
                    rb3.setBackgroundColor(Color.TRANSPARENT);
                    rb1.setBackgroundColor(Color.TRANSPARENT);
                    rb4.setTextColor(Color.BLUE);
                    rb2.setTextColor(Color.BLACK);
                    rb3.setTextColor(Color.BLACK);
                    rb1.setTextColor(Color.BLACK);
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId ==R.id.one){
                    viewPager.setCurrentItem(0);
                }else if (checkedId ==R.id.two){
                    viewPager.setCurrentItem(1);
                }else if (checkedId ==R.id.three){
                    viewPager.setCurrentItem(2);
                }else if (checkedId == R.id.four){
                    viewPager.setCurrentItem(3);
                }
            }
        });

    }
    class WeatherAdapter extends FragmentPagerAdapter{

        public WeatherAdapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {

            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

    }
}