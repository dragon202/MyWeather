package net.dragon.myweather.service;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.dragon.myweather.entity.WeatherJson;

import org.apache.commons.lang3.StringEscapeUtils;


public class WeatherManager {
    public WeatherJson getWeatherData(String cityid) throws Exception{
        OkHttpClient client = new OkHttpClient();
        String url="http://apis.juhe.cn/simpleWeather/query?city="+cityid+"&key=4f6fad1dd5e5b800cb6b8660be89b9de";
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        if (response.isSuccessful()){
            String json = response.body().string();
            String newjson = StringEscapeUtils.unescapeJava(json);
            Log.d("test",newjson);
            return JSON.parseObject(newjson,WeatherJson.class);

        }else{
            Log.d("test","网络连接错误");
            return null;
        }

    }



}
