package net.dragon.myweather.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.dragon.myweather.R;
import net.dragon.myweather.SelectActivity;
import net.dragon.myweather.dao.WeatherDao;
import net.dragon.myweather.entity.Conster;
import net.dragon.myweather.entity.Future;
import net.dragon.myweather.entity.WeatherJson;
import net.dragon.myweather.service.WeatherManager;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WeatherFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WeatherFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public WeatherFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WeatherFragment.
     */
    // TODO: Rename and change types and number of parameters
    String cityid="";
    public static WeatherFragment newInstance(String param1, String param2) {
        WeatherFragment fragment = new WeatherFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weather, container, false);
        Intent intent = getActivity().getIntent();
        cityid = intent.getStringExtra("cityid");
        ReadWeather weather = new ReadWeather();
        weather.execute(cityid);
        ImageButton setup = view.findViewById(R.id.setUp);
        setup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),SelectActivity.class);
                startActivity(intent);
            }
        });
        return view;

    }
    class ReadWeather extends AsyncTask<String,Void, WeatherJson>{


        @Override
        protected WeatherJson doInBackground(String... strings) {
            WeatherManager manager = new WeatherManager();
            try {
                Log.d("test",""+manager.getWeatherData(strings[0]));
                return manager.getWeatherData(strings[0]);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(WeatherJson weatherJson) {
            Conster.weatherJson=weatherJson;
         if ("查询成功!".equals(weatherJson.getReason())){
             TextView cityname = getView().findViewById(R.id.city);
             cityname.setText(weatherJson.getResult().getCity());
             TextView temperature = getView().findViewById(R.id.temperature);
             temperature.setText(weatherJson.getResult().getRealTime().getTemperature());
             TextView date = getView().findViewById(R.id.date);
             date.setText(weatherJson.getResult().getFuture().get(0).getDate());
             TextView info = getView().findViewById(R.id.info);
             info.setText(weatherJson.getResult().getRealTime().getInfo());
             TextView direct = getView().findViewById(R.id.direct);
             direct.setText(weatherJson.getResult().getRealTime().getDirect());
             TextView power = getView().findViewById(R.id.power);
             power.setText(weatherJson.getResult().getRealTime().getPower());
             WeatherAdapter adapter = new WeatherAdapter(weatherJson.getResult().getFuture());
             ListView listView = getView().findViewById(R.id.listview);
             listView.setAdapter(adapter);


         }else{
             Toast.makeText(getContext(),"连接错误",Toast.LENGTH_LONG).show();
         }


        }
    }
    class WeatherAdapter extends BaseAdapter{
        private List<Future> mfutures;


        public WeatherAdapter(List<Future> futures) {
            mfutures = futures;
        }

        @Override
        public int getCount() {
            return mfutures.size();
        }

        @Override
        public Object getItem(int position) {
            return mfutures.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.weather_item,null);
            Future future = mfutures.get(position);
            TextView date = view.findViewById(R.id.date);
            date.setText(future.getDate());
            TextView weather = view.findViewById(R.id.weather);
            weather.setText(future.getWeather());
            TextView direct = view.findViewById(R.id.direct);
            direct.setText(future.getDirect());
            TextView temperature = view.findViewById(R.id.temperature);
            temperature.setText(future.getTemperature());

         /* WeatherDao dao = new WeatherDao(getActivity());
          dao.saveDate(future);
         Log.d("test","保存成功");*/
            return view;
        }
    }
}