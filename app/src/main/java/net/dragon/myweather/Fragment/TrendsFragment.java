package net.dragon.myweather.Fragment;



import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;


import android.util.EventLogTags;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;


import net.dragon.myweather.R;
import net.dragon.myweather.entity.CityResult;
import net.dragon.myweather.entity.Conster;
import net.dragon.myweather.entity.Future;
import net.dragon.myweather.entity.WeatherResult;
import net.dragon.myweather.service.WeatherManager;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TrendsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TrendsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private LineChart lineChart;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public TrendsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TrendsFragment.
     */
    // TODO: Rename and change types and number of parameters

    public static TrendsFragment newInstance(String param1, String param2) {
        TrendsFragment fragment = new TrendsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trends, container, false);

        lineChart = view.findViewById(R.id.chart);
        initLineChart();


        return view;

    }

    private void initLineChart() {
        lineChart.animateXY(2000, 2000); // 呈现动画
        Legend legend = lineChart.getLegend();
        //底部注释文字颜色
        legend.setTextColor(Color.BLUE);
        setYAxis();
        //setXAxis();
        setData();
    }

    private void setYAxis() {
        YAxis yAxisLeft = lineChart.getAxisLeft();// 左边Y轴

        yAxisLeft.setDrawAxisLine(true); // 绘制Y轴
        yAxisLeft.setDrawLabels(true); // 绘制标签
        yAxisLeft.setAxisMaxValue(45); // 设置Y轴最大值
        yAxisLeft.setAxisMinValue(0); // 设置Y轴最小值
        yAxisLeft.setGranularity(2.8f); // 设置间隔尺寸
        yAxisLeft.setTextColor(Color.BLACK); //设置颜色
        yAxisLeft.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return (int)value  + "℃";
            }
        });
        // 右侧Y轴
        lineChart.getAxisRight().setEnabled(false); // 不启用
    }

    private void setXAxis(final List<String> weekStrs){
        // X轴
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setDrawAxisLine(false); // 不绘制X轴
        xAxis.setDrawGridLines(false); // 不绘制网格线
        // 模拟X轴标签数据
       //final String[] weekStrs = new String[]{"星期一", "星期二", "星期三", "星期四", "星期五", "星期六","星期日"};

        xAxis.setLabelCount(weekStrs.size()); // 设置标签数量
        xAxis.setTextColor(Color.RED); // 文本颜色
        xAxis.setTextSize(13f); // 文本大小为18dp
        xAxis.setGranularity(1f); // 设置间隔尺寸
        // 使图表左右留出点空位
        xAxis.setAxisMinimum(-0.1f); // 设置X轴最小值
        //设置颜色


       //  设置标签的显示格式
       xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return weekStrs.get((int) value);
            }
        });
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM); // 在底部显示
    }

    /**
     * 填充数据
     */
    private void setData(){
        // 模拟数据1
        List<Entry> yVals1 = new ArrayList<>();
        List<Entry> yVals2 = new ArrayList<>();

        List<String> xVals = new ArrayList<>();

        int index =0;
        for (Future f : Conster.weatherJson.getResult().getFuture()){
            System.out.println(f.getTemperature());

            xVals.add(f.getDate().substring(5));
            String s = f.getTemperature();
            s = s.replace("℃","");
            String[] arr = s.split("/");
            float low = Float.parseFloat(arr[0]);
            float high = Float.parseFloat(arr[1]);
            //"temperature": "1/7℃",
            yVals1.add(new Entry(index,low));
            yVals2.add(new Entry(index,high));
            index++;
        }
        setXAxis(xVals);


        // 2. 分别通过每一组Entry对象集合的数据创建折线数据集
        LineDataSet lineDataSet1 = new LineDataSet(yVals1, "最低温度");
        LineDataSet lineDataSet2 = new LineDataSet(yVals2, "最高温度");
        lineDataSet1.setCircleRadius(5); //设置点圆的半径
        lineDataSet2.setCircleColor(Color.RED); //设置点圆的颜色

        lineDataSet1.setDrawCircleHole(false); // 不绘制圆洞，即为实心圆点
        lineDataSet2.setCircleRadius(5); //设置点圆的半径
        // 值的字体大小为12dp
       lineDataSet1.setValueTextSize(12f);
        lineDataSet2.setDrawCircleHole(false);
        //将每一组折线数据集添加到折线数据中
        LineData lineData = new LineData(lineDataSet1,lineDataSet2);
        //设置折线颜色
        lineDataSet2.setColor(Color.RED);
        lineDataSet2.setValueTextSize(12f);
        //折线上的文字
        lineData.setValueTextColor(Color.BLACK);
        //将折线数据设置给图表
        lineChart.setData(lineData);

    }




}
