package net.dragon.myweather.Fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.dragon.myweather.R;
import net.dragon.myweather.entity.WeatherResult;
import net.dragon.myweather.service.MeteorologyManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MeteorologyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MeteorologyFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MeteorologyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MeteorologyFragment.
     */
    // TODO: Rename and change types and number of parameters
    String cityid="";
    public static MeteorologyFragment newInstance(String param1, String param2) {
        MeteorologyFragment fragment = new MeteorologyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
             View view = inflater.inflate(R.layout.fragment_meteorology, container, false);
            Intent intent = getActivity().getIntent();
            cityid = intent.getStringExtra("cityid");
            ReadMeteDate readMeteDate = new ReadMeteDate();
            readMeteDate.execute(cityid);
            return view;

    }
    class ReadMeteDate extends AsyncTask<String,Void, WeatherResult>{

        @Override
        protected WeatherResult doInBackground(String... strings) {
            MeteorologyManager manager = new MeteorologyManager();
            try {
                return manager.getWeatherMData(strings[0]);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(WeatherResult weatherResult) {

            if (weatherResult.getError_code()==0){
                TextView kone = getView().findViewById(R.id.kone);
                TextView ktwo = getView().findViewById(R.id.ktwo);
                if (weatherResult.getResult().getLife().getKongtiao()!=null){
                    kone.setText(weatherResult.getResult().getLife().getKongtiao().getV());
                    ktwo.setText(weatherResult.getResult().getLife().getKongtiao().getDes());
                }else{

                }

                TextView shuone = getView().findViewById(R.id.shuone);
                TextView shutwo = getView().findViewById(R.id.shutwo);
                if (weatherResult.getResult().getLife().getShushidu()!=null){
                    shuone.setText(weatherResult.getResult().getLife().getShushidu().getV());
                    shutwo.setText(weatherResult.getResult().getLife().getShushidu().getDes());
                }

                TextView chuanone = getView().findViewById(R.id.chuanone);
                TextView chuantwo = getView().findViewById(R.id.chuantwo);
                if (weatherResult.getResult().getLife().getChuangyi()!=null){
                    chuanone.setText(weatherResult.getResult().getLife().getChuangyi().getV());
                    chuantwo.setText(weatherResult.getResult().getLife().getChuangyi().getDes());
                }

                TextView ganone = getView().findViewById(R.id.ganone);
                TextView gantwo = getView().findViewById(R.id.gantwo);
                if (weatherResult.getResult().getLife().getGanmao()!=null){
                    ganone.setText(weatherResult.getResult().getLife().getGanmao().getV());
                    gantwo.setText(weatherResult.getResult().getLife().getGanmao().getDes());
                }

                TextView yunone = getView().findViewById(R.id.yunone);
                TextView yuntwo = getView().findViewById(R.id.yuntwo);
                if (weatherResult.getResult().getLife().getYundong()!=null){
                    yunone.setText(weatherResult.getResult().getLife().getYundong().getV());
                    yuntwo.setText(weatherResult.getResult().getLife().getYundong().getDes());
                }

            }else{
                Toast.makeText(getContext(),"连接错误",Toast.LENGTH_LONG).show();
            }
            super.onPostExecute(weatherResult);
        }
    }
}